#!/usr/bin/python
#-*- coding: utf-8 -*-

# Cifrado del César
# (vnm3@alu.ua.es)

import sys
import os

# Declaración de alfabetos:
# Alfabeto de caracteres soportados
charToNumber = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
                                'K': 10, 'L': 11, 'M': 12, 'N': 13, 'Ñ': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19,
                                'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26, ' ': 27}

# Generamos un alfabeto complementario para obtener letras a partir de su posición
numberToChar = {}
for key, value in charToNumber.items():
    numberToChar[value] = key


# Funćión que aplica el cifrado cesar a un texto con el desplazamiento indicado
def cesar(textInput, desplazamiento):
    # Conversión del texto a mayúsculas
    textInputUpper = textInput.upper()

    # Conversión del texto aplicando el desplazamiento
    result = ""
    for i in textInputUpper:
        # Comprobamos que el valor se encuentra en nuestro alfabeto
        if i in charToNumber:
            pos = charToNumber[i]
            newPos = (pos + desplazamiento) % len(numberToChar)
            result = result + numberToChar[newPos]

    return result


# Main
def main():
    # Obtenemos el número de parámetros con los que se ha ejecutado el programa
    numParam = len(sys.argv)

    # Comprobamos que el número de parámetros sea el esperado
    if numParam == 2 or numParam == 3:

        # Comprobamos que el segundo parámetro es un número
        paramDesplazamiento = sys.argv[1]
        if paramDesplazamiento.isdigit():

            # Si solo se ha ejecutado con un parámetro extra, pedimos un texto para convertir
            if numParam == 2:
                # Solicitamos el texto al usuario
                textInput = input("Escribe el texto a convertir: ")
                # Aplicamos el desplazamiento al texto
                result = cesar(textInput, int(paramDesplazamiento))
                # Mostramos el resultado
                print("> Resultado: " + result)

            # Si se ha ejecutado con dos parámetros extra, debemos leer un fichero para convertir
            if numParam == 3:
                # Recogemos el nombre del fichero a convertir
                nombreFichero = sys.argv[2]

                # Comprobamos que el fichero indicado existe
                if os.path.exists(nombreFichero):
                    # Abrimos el fichero indicado
                    with open(nombreFichero) as f:
                        # Leemos el fichero y aplicamos el cifrado a cada línea
                        print("Resultado:")
                        for line in f:
                            # Aplicamos el desplazamiento al texto
                            result = cesar(line, int(paramDesplazamiento))
                            # Mostramos el resultado
                            print(result)
                else:
                    print("# Error: El fichero indicado no existe.")

        else:
            print("# Error: El argumento indicado no es un número valido.")

    else:
        # Mostramos un mensaje de error al usuario en caso contrario
        print("# Error: El número de argumentos es incorrecto.")


if __name__ == '__main__':
    main()
